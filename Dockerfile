FROM debian:latest

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV PATH /opt/conda/bin:$PATH

RUN apt-get update --fix-missing && \
    apt-get install -y wget bzip2 ca-certificates \
    libglib2.0-0 libxext6 libsm6 libxrender1 libxft-dev libfreetype6 libfreetype6-dev \
    git vim

# install miniconda
RUN wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p /opt/conda && \
    rm ~/miniconda.sh && \
    conda update conda -y && \
    conda install python==3.6.9 && \
    conda install -c conda-forge pip && \
    conda install -c conda-forge ocw && \
    conda install pandas -y && \
    pip install geojsoncontour && \
    pip install pyyaml && \
    conda update basemap -y && \
    conda install -c conda-forge netcdftime -y && \
    conda install libnetcdf -y && \
    conda install netcdf4 -y && \
    conda install netcdftime -y && \
    conda install basemap-data-hires -y && \
    conda clean -y --all

ENV PYTHONPATH="/usr/local/climate::/usr/local/climate/ocw" \
    PROJ_LIB="/opt/conda/share/proj"

WORKDIR /home
